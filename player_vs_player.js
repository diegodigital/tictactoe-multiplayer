/*
    These variables handles the points for user and cpu.
    They're attached to the window object so they'll keep the values
        on restart.
*/

window.player1 = window.player1 || 0;
window.player2 = window.player2 || 0;

/**
 * Requires section
 * here I require all the necessary libraries for the game
 */

// var NextPlayerPlays = require('./libraries/next_player_plays.js');
var DrawPlayerTwoMovement = require('./libraries/draw_player_two_movement.js');
var DrawPlayerTwoDrag = require('./libraries/draw_player_two_drag.js');
var WinnerCheck = require('./libraries/winner_check.js');

DrawMod = require('./../common/draw_figures.js');

/**
 * Socket io request functions
 * the "socket" variable contains the socket object from the raw.html file
 */
socket.on('send message', function(data) {
    console.log(data.hello);
});


// show connected users
var connected;

socket.on('connected users', function(data) {
    console.log('Conectados: ' + data.q);
    connected = data.q;
    // show the connected users on top
});


// socket.on('player moves', function(data) {
//     console.log('about to call randomcpukeep....');
//     RandomCpuPiece(data.piece);
// });

socket.on('ihrmovement', function(data) {
    // console.log('reciving movement from other player');
    // console.log(data);
    DrawPlayerTwoMovement(data.playerPiece, data.piecePosition);
    setTimeout(PVsPGame.winnerCheck(), 500);
});

socket.on('ihrdragged', function(data) {
    console.log('reciving dragged from other player');
    console.log(data);
    DrawPlayerTwoDrag(data.piece, data.whereItWas, data.whereItGo);
    // setTimeout(PVsPGame.winnerCheck(), 500);
});

PVsPGame = {
    handleResize: function(){
        var w = window.innerWidth;
        var h = window.innerHeight;
        this.game.width = w;
        this.game.height = h;
        var ratio = 1600/900; 
        var windowRatio = w/h;
        RATIO = w/1600;
        if (windowRatio > TaTeTi._RATIO) {
            TaTeTi._RATIO = h/900;
        }
    },

    create: function() {
        this.handleResize();
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.preloadBG = this.add.sprite(0,0,'generalBG');
        this.preloadBG.width = TaTeTi._WIDTH;
        this.preloadBG.height = TaTeTi._HEIGHT;
        this.board = this.add.sprite(TaTeTi._WIDTH/2+TaTeTi._RATIO*30,TaTeTi._HEIGHT/2,'board');
        this.board.anchor.set(0.5,0.5);
        this.board.scale.setTo(TaTeTi._RATIO*0.35,TaTeTi._RATIO*0.35);
        this.board.name = 'THE BOARD';
        TaTeTi._GAMESTARTED = true;
        this.HitZoneGroup = this.add.group();

        // the group for player pieces
        game.playerPieces = this.add.group();
        // the group for cpu pieces
        game.cpuPieces = this.add.group();

        // display score
        this.showUserScore = this.add.text(TaTeTi._WIDTH/1.3+TaTeTi._RATIO*30,TaTeTi._HEIGHT/1.5, 'Player1: ' + window.userScore, { fontSize: '28px', fill: '#000' });
        this.showCpuScore = this.add.text(TaTeTi._WIDTH/1.3+TaTeTi._RATIO*30,TaTeTi._HEIGHT/1.3, 'Player2: ' + window.cpuScore, { fontSize: '28px', fill: '#000' });

        // display online players
        this.showConnectedUsers = this.add.text(TaTeTi._WIDTH/1.3+TaTeTi._RATIO*30,TaTeTi._HEIGHT/2.2, 'conectados: ' + connected, { fontSize: '20px', fill: '#FFF' });

        this.initPlaces();
        for(var i = 0; i < _COORDINATES.length; i++){
            // create these variables dynamicallly like: "this.square0", "this.square1", etc
            eval("this.square" + i + " = this.add.sprite(0,0,'square')");
            eval("this.square" + i + ".anchor.set(0.5,0.5)");
            eval("this.square" + i + ".alpha = 0");
            eval("this.square" + i + ".x = _COORDINATES[i]._x1");
            eval("this.square" + i + ".y = _COORDINATES[i]._y1");
            eval("this.square" + i + ".name = _COORDINATES[i]._ID");
            eval("this.square" + i + ".inputEnabled = true");
            eval("this.square" + i + ".scale.setTo(TaTeTi._RATIO*0.4,TaTeTi._RATIO*0.4)");
            eval("this.square" + i + ".events.onInputDown.add(this.onDown,this)");
            this.HitZoneGroup.add(eval("this.square" + i));
        }
        game.physics.enable(this.HitZoneGroup, Phaser.Physics.ARCADE);
        game.physics.enable(game.playerPieces, Phaser.Physics.ARCADE);
        game.physics.enable(game.cpuPieces, Phaser.Physics.ARCADE);
    },

    drawPiece: function(posX,posY,name){
        // if the game has ended then I'll skip this function
        if(TaTeTi._GAMESTARTED == false) return;

        // this portion of code runs only the first time
        if(TaTeTi._FIRSTCLICK){
            // asign a random value on every start: 0 for o, 1 for x
            var num = Math.floor(Math.random() * 2);
            if(num == 0){
                var self = this;

                // assign the new piece to first player
                Piece[TaTeTi._COUNT]._VALUE = 0;
                Piece[TaTeTi._COUNT]._OLD_PLACE_X = posX;
                Piece[TaTeTi._COUNT]._OLD_PLACE_Y = posY;
                Piece[TaTeTi._COUNT]._IS_PLAYER = true;
                PiecesPosition[name]._PIECEVAL = 'CIRCLE';
                Piece[TaTeTi._COUNT]._CURRENT_POS = name;
                TaTeTi._PLAYERPIECE = 0;
                TaTeTi._PLAYER2PIECE = 1;

                // draws the circle
                this.theCircle = DrawMod.drawCircle(posX,posY,TaTeTi._CIRCLES);

                // decrease the number of pieces available
                TaTeTi._CIRCLES--;
                // add the piece to the board
                game.playerPieces.add(this.theCircle);

                setTimeout(function(){
                    TaTeTi._COUNT++;
                    self.emitMyMovement(TaTeTi._PLAYERPIECE, name);
                    // self.nextPlayerPlays(TaTeTi._CPUPIECE);
                },200);
            }else if(num == 1){
                var self = this;

                // assing the new piece
                Piece[TaTeTi._COUNT]._VALUE = 1;
                Piece[TaTeTi._COUNT]._OLD_PLACE_X = posX;
                Piece[TaTeTi._COUNT]._OLD_PLACE_Y = posY;
                Piece[TaTeTi._COUNT]._IS_PLAYER = true;
                PiecesPosition[name]._PIECEVAL = 'CROSS';
                Piece[TaTeTi._COUNT]._CURRENT_POS = name;
                TaTeTi._PLAYERPIECE = 1;
                TaTeTi._PLAYER2PIECE = 0;

                // draws the x
                this.theCross = DrawMod.drawCross(posX,posY,TaTeTi._CROSSES);

                // decrease the number of pieces available
                TaTeTi._CROSSES--;
                // add the piece to the board
                game.playerPieces.add(this.theCross);

                setTimeout(function(){
                    TaTeTi._COUNT++;
                    self.emitMyMovement(TaTeTi._PLAYERPIECE, name);
                    // self.nextPlayerPlays(TaTeTi._CPUPIECE);
                },200);
            }
            TaTeTi._FIRSTCLICK = false;
        }else{
            //
            // this portion of code runs after the first move
            //
            if(TaTeTi._PLAYERPIECE == 0){ // draw the circle
                Piece[TaTeTi._COUNT]._VALUE = 0;
                Piece[TaTeTi._COUNT]._OLD_PLACE_X = posX;
                Piece[TaTeTi._COUNT]._OLD_PLACE_Y = posY;
                Piece[TaTeTi._COUNT]._IS_PLAYER = true;
                PiecesPosition[name]._PIECEVAL = 'CIRCLE';
                Piece[TaTeTi._COUNT]._CURRENT_POS = name;
                var self = this;

                // draws the circle
                console.log(DrawMod);
                this.theCircle = DrawMod.drawCircle(posX,posY,TaTeTi._CIRCLES);

                game.playerPieces.add(this.theCircle);
                TaTeTi._CIRCLES--;

                // loop function
                setTimeout(function(){
                    TaTeTi._COUNT++;
                    self.emitMyMovement(TaTeTi._PLAYERPIECE, name);
                    // self.nextPlayerPlays(TaTeTi._CPUPIECE);
                    if(TaTeTi._CIRCLES == 0){
                        self.disableSquareClick();
                    }
                },1000);
            }else if(TaTeTi._PLAYERPIECE == 1){
                Piece[TaTeTi._COUNT]._VALUE = 1;
                Piece[TaTeTi._COUNT]._OLD_PLACE_X = posX;
                Piece[TaTeTi._COUNT]._OLD_PLACE_Y = posY;
                Piece[TaTeTi._COUNT]._IS_PLAYER = true;
                PiecesPosition[name]._PIECEVAL = 'CROSS';
                Piece[TaTeTi._COUNT]._CURRENT_POS = name;
                var self = this;

                // draws the x
                console.log(DrawMod);
                this.theCross = DrawMod.drawCross(posX,posY,TaTeTi._CROSSES);

                game.playerPieces.add(this.theCross);
                TaTeTi._CROSSES--;
                setTimeout(function(){
                    TaTeTi._COUNT++;
                    self.emitMyMovement(TaTeTi._PLAYERPIECE, name);
                    // self.nextPlayerPlays(TaTeTi._CPUPIECE);
                    if(TaTeTi._CROSSES == 0){
                        self.disableSquareClick();
                    }
                },1000);
            }
        }
    },

    disableSquareClick: function(){
        this.HitZoneGroup.setAll('inputEnabled',false);
        TaTeTi._ALLPIECESINPLACE = true;
    },

    squaresClickable: function(boolValue){
        // this.HitZoneGroup.setAll('inputEnabled', true);
        // TaTeTi._ALLPIECESINPLACE = true;

        for(var i = 0; i < _COORDINATES.length; i++){
            eval("this.square" + i + ".inputEnabled = boolValue");
        }

        console.log('all defrost: ' + boolValue);
    },

    /**
     * this handles the second player moving
     * the first player should not be allowed to move until
     * the second moves
     */
    // nextPlayerPlays: function(piece) {

    //     game.playerPieces.setAll('inputEnabled',false);

    //     this.movePiece(piece);

    //     NextPlayerPlays();

    // },

    // returns a value between val1 (included) and val2 (excluded)
    getRandom: function(val1,val2){
        return Math.floor(Math.random() * (val2 - val1) + val1);
    },

    onDown: function(theSquare,pointer){
        this.controlPlace(theSquare);
        game.playerPieces.add(theSquare);
    },
    initPlaces: function(){
        _COORDINATES = [
            { _x1: this.board.x-TaTeTi._RATIO*90,
              _y1: this.board.y-TaTeTi._RATIO*90, 
              _TAKEN: false, 
              _ID: '0' 
            },
            {
             _x1: this.board.x-TaTeTi._RATIO*5,
             _y1: this.board.y-TaTeTi._RATIO*90,
             _TAKEN: false,
             _ID: '1' 
            },
            {
             _x1: this.board.x+TaTeTi._RATIO*75,
             _y1: this.board.y-TaTeTi._RATIO*100,
             _TAKEN: false, 
             _ID: '2' 
            },
            { 
             _x1: this.board.x-TaTeTi._RATIO*90, 
             _y1: this.board.y+TaTeTi._RATIO*10, 
             _TAKEN: false, 
             _ID: '3' 
            },
            { 
             _x1: this.board.x-(this.board.width/2)+TaTeTi._RATIO*TaTeTi._SPACE1, 
             _y1: this.board.y-(this.board.height/2)+TaTeTi._RATIO*TaTeTi._SPACE1, 
             _TAKEN: false, 
             _ID: '4' 
            },
            { 
             _x1: this.board.x+TaTeTi._RATIO*80, 
             _y1: this.board.y-TaTeTi._RATIO*10, 
             _TAKEN: false, 
             _ID: '5'
            },
            { 
             _x1: this.board.x-TaTeTi._RATIO*90, 
             _y1: this.board.y+TaTeTi._RATIO*100, 
             _TAKEN: false, 
             _ID: '6' 
            },
            { 
             _x1: this.board.x+TaTeTi._RATIO*10, 
             _y1: this.board.y+TaTeTi._RATIO*90, 
             _TAKEN: false, 
             _ID: '7' 
            },
            { 
             _x1: this.board.x+TaTeTi._RATIO*95, 
             _y1: this.board.y+TaTeTi._RATIO*80, 
             _TAKEN: false, 
             _ID: '8' 
            }
        ];
    },

    controlPlace: function(square){
        // search for a winner ONLY after the 5th move
        if (TaTeTi._COUNT > 4) { this.winnerCheck(); }

        if (TaTeTi._ALLPIECESINPLACE){
            this.winnerCheck();
            this.dragGame(square);
            // this.newDragGame(square);
        } else {
            var name = square.name;
            var posX = square.x;
            var posY = square.y;
            if(!_COORDINATES[name]._TAKEN){
                this.drawPiece(posX,posY,name);
                _COORDINATES[name]._TAKEN = true;
            } else {
                console.log('OCUPIED!!');
            }
        }
        
    },

    dragGame: function(){
        for(var i = 0; i < Piece.length; i++){
            // debugger;
            if(Piece[i]._IS_PLAYER){
                game.physics.arcade.enable(game.playerPieces.children[i]);
                game.playerPieces.children[i].name = Piece[i]._NAME;
                game.playerPieces.children[i].place = Piece[i]._CURRENT_POS;
                game.playerPieces.children[i].inputEnabled = true;
                game.playerPieces.children[i].events.onInputDown.add(this.onDownForDrag,this);
            }else if(game.playerPieces.children[i].key == 'square'){
                game.playerPieces.children[i].inputEnabled = false;
            }
        }

        for (var i = 0; i < game.cpuPieces.length; i++) {
            if(Piece[i]._IS_PLAYER !== true){
                game.physics.arcade.enable(game.cpuPieces.children[i]);
                game.cpuPieces.children[i].name = Piece[i]._NAME;
                game.cpuPieces.children[i].place = Piece[i]._CURRENT_POS;
                game.cpuPieces.children[i].inputEnabled = true;
                game.cpuPieces.children[i].events.onInputDown.add(this.onDownForDrag,this);
            }else if(game.playerPieces.children[i].key == 'square'){
                game.cpuPieces.children[i].inputEnabled = false;
            }
        };
    },

    onDownForDrag: function(theDragged){
        var data = theDragged.place;
        this.available(data,theDragged);
    },

    available: function(pos,theDragged){
        if(pos == 0){
            if(!_COORDINATES[1]._TAKEN || !_COORDINATES[3]._TAKEN || !_COORDINATES[4]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 1){
            if(!_COORDINATES[0]._TAKEN || !_COORDINATES[2]._TAKEN || !_COORDINATES[4]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 2){
            if(!_COORDINATES[1]._TAKEN || !_COORDINATES[4]._TAKEN || !_COORDINATES[5]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 3){
            if(!_COORDINATES[0]._TAKEN || !_COORDINATES[4]._TAKEN || !_COORDINATES[6]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 4){
            if(!_COORDINATES[0]._TAKEN || !_COORDINATES[1]._TAKEN || !_COORDINATES[2]._TAKEN || !_COORDINATES[3]._TAKEN || !_COORDINATES[5]._TAKEN || !_COORDINATES[6]._TAKEN || !_COORDINATES[7]._TAKEN || !_COORDINATES[8]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 5){
            if(!_COORDINATES[2]._TAKEN || !_COORDINATES[4]._TAKEN || !_COORDINATES[8]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 6){
            if(!_COORDINATES[3]._TAKEN || !_COORDINATES[4]._TAKEN || !_COORDINATES[7]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 7){
            if(!_COORDINATES[4]._TAKEN || !_COORDINATES[6]._TAKEN || !_COORDINATES[8]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }else if(pos == 8){
            if(!_COORDINATES[4]._TAKEN || !_COORDINATES[5]._TAKEN || !_COORDINATES[7]._TAKEN){
                theDragged.input.enableDrag(true);
                theDragged.originalPlace = pos;
                theDragged.events.onDragStart.add(this.onDragStart, this);
                theDragged.events.onDragStop.add(this.onDragStop, this);
            }
        }

    },

    onDragStart: function(thePiece){
        this.original_pos_x = thePiece.x;
        this.original_pos_y = thePiece.y;
    },

    onDragStop: function(thePiece){
        // check if the piece is out of bounds
        if(thePiece.x < (this.board.x-this.board.width/2) ||
            thePiece.x > (this.board.x+this.board.width/2) ||
            thePiece.y < (this.board.y-this.board.height/2) ||
            thePiece.y > (this.board.y+this.board.height/2))
        {
            // return the piece to it´s original position
            thePiece.x = _COORDINATES[parseInt(thePiece.originalPlace)]._x1;
            thePiece.y = _COORDINATES[parseInt(thePiece.originalPlace)]._y1;
        }else{
            game.physics.arcade.overlap(thePiece, this.HitZoneGroup, this.test, null, this);
        }
    },

    onPieceDrag: function(piece){
        if( piece.input.isDragged ){
            if( piece.body != null ){
                piece.body.x = this.input.activePointer.worldX;
                piece.body.y = this.input.activePointer.worldY;
            }
        }
    },

    test: function(thePiece,theGroup){
        this.availableSiblings(thePiece,theGroup);

    },

    /*
        the piece can only be moved to a sibling cell and/or
        to a cell that wasn't occupied in the last opponent move
    */
    availableSiblings: function(thePiece,theGroup){
        var self = this,
            whereIcome = thePiece.originalPlace,
            whereIam = theGroup.name;

        // check available cells
        self.whereCanIgo(whereIcome);

        if (
            !_COORDINATES[whereIam]._TAKEN && // not taken
            TaTeTi._AVAILABLE[whereIam] == true // is valid place to move
        ) {
            console.info("Player movement: \n where I come: " + whereIcome + "\n where I am now: " + whereIam + "\n item in Piece: " + thePiece.name);

            thePiece.x = _COORDINATES[whereIam]._x1;
            thePiece.y = _COORDINATES[whereIam]._y1;
            _COORDINATES[whereIam]._TAKEN = true;
            _COORDINATES[whereIcome]._TAKEN = false;

            // update the Original Place
            thePiece.originalPlace = "" + whereIam;
            PiecesPosition[whereIcome]._PIECEVAL = null;

            PiecesPosition[whereIam]._PIECEVAL = (TaTeTi._PLAYERPIECE == 0) ? 'CIRCLE' : 'CROSS';

            /*
                This need some refactor because is super ugly
             */
            thePiece.place = "" + whereIam;
            /*
                +++++++++++++++++++++++++++++++++++++++++++++
            */

            var name = parseInt(thePiece.name);
            Piece[name - 1]._CURRENT_POS = "" + whereIam;

            console.info("That was valid");
            // if it was valid the in check for winners again
            this.winnerCheck();

            // debugger;

            this.emitMyDragged(name - 1, parseInt(whereIcome), parseInt(whereIam));

            // this.cpuPlayHard();
        } else {
            thePiece.x = _COORDINATES[whereIcome]._x1;
            thePiece.y = _COORDINATES[whereIcome]._y1;
            console.info("That was invalid");
        }
    },

    /**
     * available places accoring to the piece position
     */
    whereCanIgo: function(thePiece){
        // console.log("whereCanIgo: " + thePiece);
        // console.log("Check TaTeTi._AVAILABLE state");
        if (thePiece == 0) {
            TaTeTi._AVAILABLE[0] = false;
            TaTeTi._AVAILABLE[1] = true;
            TaTeTi._AVAILABLE[2] = false;
            TaTeTi._AVAILABLE[3] = true;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = false;
            TaTeTi._AVAILABLE[6] = false;
            TaTeTi._AVAILABLE[7] = false;
            TaTeTi._AVAILABLE[8] = false;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 1){
            TaTeTi._AVAILABLE[0] = true;
            TaTeTi._AVAILABLE[1] = false;
            TaTeTi._AVAILABLE[2] = true;
            TaTeTi._AVAILABLE[3] = false;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = false;
            TaTeTi._AVAILABLE[6] = false;
            TaTeTi._AVAILABLE[7] = false;
            TaTeTi._AVAILABLE[8] = false;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 2) {
            TaTeTi._AVAILABLE[0] = false;
            TaTeTi._AVAILABLE[1] = true;
            TaTeTi._AVAILABLE[2] = false;
            TaTeTi._AVAILABLE[3] = false;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = true;
            TaTeTi._AVAILABLE[6] = false;
            TaTeTi._AVAILABLE[7] = false;
            TaTeTi._AVAILABLE[8] = false;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 3) {
            TaTeTi._AVAILABLE[0] = true;
            TaTeTi._AVAILABLE[1] = false;
            TaTeTi._AVAILABLE[2] = false;
            TaTeTi._AVAILABLE[3] = false;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = false;
            TaTeTi._AVAILABLE[6] = true;
            TaTeTi._AVAILABLE[7] = false;
            TaTeTi._AVAILABLE[8] = false;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 4) {
            TaTeTi._AVAILABLE[0] = true;
            TaTeTi._AVAILABLE[1] = true;
            TaTeTi._AVAILABLE[2] = true;
            TaTeTi._AVAILABLE[3] = true;
            TaTeTi._AVAILABLE[4] = false;
            TaTeTi._AVAILABLE[5] = true;
            TaTeTi._AVAILABLE[6] = true;
            TaTeTi._AVAILABLE[7] = true;
            TaTeTi._AVAILABLE[8] = true;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 5) {
            TaTeTi._AVAILABLE[0] = false;
            TaTeTi._AVAILABLE[1] = false;
            TaTeTi._AVAILABLE[2] = true;
            TaTeTi._AVAILABLE[3] = false;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = false;
            TaTeTi._AVAILABLE[6] = false;
            TaTeTi._AVAILABLE[7] = false;
            TaTeTi._AVAILABLE[8] = true;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 6) {
            TaTeTi._AVAILABLE[0] = false;
            TaTeTi._AVAILABLE[1] = false;
            TaTeTi._AVAILABLE[2] = false;
            TaTeTi._AVAILABLE[3] = true;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = false;
            TaTeTi._AVAILABLE[6] = false;
            TaTeTi._AVAILABLE[7] = true;
            TaTeTi._AVAILABLE[8] = false;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 7) {
            TaTeTi._AVAILABLE[0] = false;
            TaTeTi._AVAILABLE[1] = false;
            TaTeTi._AVAILABLE[2] = false;
            TaTeTi._AVAILABLE[3] = false;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = false;
            TaTeTi._AVAILABLE[6] = true;
            TaTeTi._AVAILABLE[7] = false;
            TaTeTi._AVAILABLE[8] = true;
            return TaTeTi._AVAILABLE;
        } else if (thePiece == 8) {
            TaTeTi._AVAILABLE[0] = false;
            TaTeTi._AVAILABLE[1] = false;
            TaTeTi._AVAILABLE[2] = false;
            TaTeTi._AVAILABLE[3] = false;
            TaTeTi._AVAILABLE[4] = true;
            TaTeTi._AVAILABLE[5] = true;
            TaTeTi._AVAILABLE[6] = false;
            TaTeTi._AVAILABLE[7] = true;
            TaTeTi._AVAILABLE[8] = false;
            return TaTeTi._AVAILABLE;
        }
    },

    /*
     * check who's the winner and start the state "Winner"
     */
    winnerCheck: function() {

        WinnerCheck();

    },

    /*
        this helps to show the winner after a timeout setted in
        the function above (winnerCheck)
    */
    winnerAlert: function(winner) {},

    emitMyMovement: function(player, position) {
        console.log('emitting my movement');
        // PVsPGame.disableSquareClick();
        PVsPGame.squaresClickable(false);
        socket.emit('mymovement', {
            playerPiece: player,
            piecePosition: position
        });

        setTimeout(PVsPGame.winnerCheck(), 500);
    },

    emitMyDragged: function(name, whereItWas, whereItGo) {
        console.log('emitting my dragged piece');
        PVsPGame.squaresClickable(false);
        socket.emit('mydragged', {
            piece: name,
            whereItWas: whereItWas,
            whereItGo: whereItGo
        });

        PVsPGame.squaresClickable(false);
    },

    movePiece: function() {
        // debugger;
        socket.emit('yourturn', { move: true });
    },
};

module.exports = PVsPGame;