/*
 * The CPU will keep playing randomly after movement number 6
 * in which all pieces are on the board
*/
var drawPlayerTwoDrag = function(piece, whereItWas, whereItGo) {

    self = this;

    // get a random cpu piece to move
    var piece = piece,
        moved = false,
        playerTwoPiece;

    // debugger;

    // get the other player piece and relate it with the "locals" 
    (piece==0 || piece==1) ? playerTwoPiece=0 : (piece==2 || piece==3) ? playerTwoPiece=1 : playerTwoPiece=2;

    // save the original cpu piece position
    var original_position = Piece[piece]._CURRENT_POS;

    // debugger;
    // update with the new position from the coordinates
    game.cpuPieces.children[playerTwoPiece].x = _COORDINATES[whereItGo]._x1;
    game.cpuPieces.children[playerTwoPiece].y = _COORDINATES[whereItGo]._y1;
    // clean the now empty coordinate
    _COORDINATES[original_position]._TAKEN = false;
    // ocupy the new coordinate
    _COORDINATES[whereItGo]._TAKEN = true;
    // update pieces position old cell to empty value
    PiecesPosition[whereItWas]._PIECEVAL = null;
    // update pieces position value
    PiecesPosition[whereItGo]._PIECEVAL = (TaTeTi._PLAYER2PIECE == 0) ? 'CIRCLE' : 'CROSS';
    // update Piece position
    Piece[piece]._CURRENT_POS = "" + whereItGo;

    moved = true;
    PVsCPUGame.winnerCheck();

    // debugger;

    PVsPGame.squaresClickable(true);

}

module.exports = drawPlayerTwoDrag;