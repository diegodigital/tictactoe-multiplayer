/*
 * Draw the piece movement from the player 2
 * it accepts the number of player and piece position
 */
var drawPlayerTwoMovement = function(playerPiece, piecePosition) {
    console.log('drawing player two movement');
    var num = piecePosition;
    if(!_COORDINATES[num]._TAKEN){

        TaTeTi._FIRSTCLICK = false;
        TaTeTi._PLAYER2PIECE = playerPiece;

        if (playerPiece == 1) {
            TaTeTi._PLAYERPIECE = 0;
            var posX = _COORDINATES[num]._x1;
            var posY = _COORDINATES[num]._y1;
            PiecesPosition[num]._PIECEVAL = 'CROSS';

            // add the piece to the group
            game.cpuPieces.add(DrawMod.drawCross(posX,posY));

            _COORDINATES[num]._TAKEN = true;
            Piece[TaTeTi._COUNT]._VALUE = 1;
            Piece[TaTeTi._COUNT]._OLD_PLACE_X = posX;
            Piece[TaTeTi._COUNT]._OLD_PLACE_Y = posY;

            Piece[TaTeTi._COUNT]._CURRENT_POS = "" + (num);

            TaTeTi._COUNT++;
            // console.log("added random piece");
        } else if(playerPiece == 0) {
            TaTeTi._PLAYERPIECE = 1;
            var posX = _COORDINATES[num]._x1;
            var posY = _COORDINATES[num]._y1;
            PiecesPosition[num]._PIECEVAL = 'CIRCLE';
            // add the piece to the group
            game.cpuPieces.add(DrawMod.drawCircle(posX,posY));

            _COORDINATES[num]._TAKEN = true;
            Piece[TaTeTi._COUNT]._VALUE = 0;
            Piece[TaTeTi._COUNT]._OLD_PLACE_X = posX;
            Piece[TaTeTi._COUNT]._OLD_PLACE_Y = posY;

            Piece[TaTeTi._COUNT]._CURRENT_POS = "" + (num);

            TaTeTi._COUNT++;
            // console.log("added random piece");
        }

        // PVsPGame.HitZoneGroup.setAll('inputEnabled',true);
        PVsPGame.squaresClickable(true);
    }
}

module.exports = drawPlayerTwoMovement;