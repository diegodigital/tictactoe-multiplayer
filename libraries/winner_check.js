/*
 * check who's the winner and start the state "WinnerMultiplayer"
 */
var winnerCheck = function() {
    self = this;
    var circle = 0,
        ex = 0;

    winState.forEach(function(value) {
        value.forEach(function(cell) {
            if (PiecesPosition[cell - 1]._PIECEVAL == 'CIRCLE')        circle++;
            else if (PiecesPosition[cell - 1]._PIECEVAL == 'CROSS')    ex++;

            if(circle == 3) {
                // first stop the game from keep drawing
                TaTeTi._GAMESTARTED = false;
                console.info("circles q: " + circle + "\nexes q: " + ex);

                // timeout = window.setTimeout(declareWinner, 3000);
                // function declareWinner() {
                // assign points to global variables for user and cpu
                if(TaTeTi._PLAYERPIECE == 0) {
                    console.log("Player wins with \"o\"");

                    window.userScore++;

                    setTimeout(function() {
                        game.state.start("WinnerMultiplayer", true, false, "user");
                    }, 500)

                } else if (TaTeTi._PLAYERPIECE == 1) {
                    console.log("Cpu wins with \"o\"");

                    window.cpuScore++;

                    setTimeout(function() {
                        game.state.start("WinnerMultiplayer", true, false, "cpu");
                    }, 500)
                };
                // console.info(" user: " + window.userScore + "\n cpu: " + window.cpuScore);
                // }
            } else if (ex == 3) {
                // first stop the game from keep drawing
                TaTeTi._GAMESTARTED = false;
                // console.info("exes q: " + ex + "\ncircles q: " + circle);

                // timeout = window.setTimeout(declareWinner, 3000);
                // function declareWinner() {
                // assign points to global variables for user and cpu
                if (TaTeTi._PLAYERPIECE == 1) {
                    console.log("Player wins with \"x\"");

                    window.userScore++;

                    setTimeout(function() {
                        game.state.start("WinnerMultiplayer", true, false, "user");
                    }, 500)

                } else if (TaTeTi._PLAYERPIECE == 0) {
                    console.log("Cpu wins with \"x\"");

                    window.cpuScore++;

                    setTimeout(function() {
                        game.state.start("WinnerMultiplayer", true, false, "cpu");
                    }, 500)
                }
                // }
            }
        });
        // reset the variables to start a new check
        circle = 0; ex = 0;
    });
    // console.info("Score:\n user: " + window.userScore + "\n cpu: " + window.cpuScore);
}

module.exports = winnerCheck;